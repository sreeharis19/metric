import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Login from "./pages/login";
import Index from "./pages/index";
import Register from "./pages/register";
import FileUploadPage from "./pages/fileupload";

class App extends Component {

    render() {
        return (
            <div className="App">
                <Router>
                    <Switch>
                        <Route exact path='/' component={Login} />
                        <Route path='/index' component={Index}/>
                        <Route path='/register' component={Register} />
                        <Route path='/fileupload/' component={FileUploadPage} />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;

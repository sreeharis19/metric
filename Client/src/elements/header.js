import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import TitleComponent from "../pages/title";


export default class Header extends Component {


    state = {
        toDashboard: false,
    };

    render() {
        if (this.state.toDashboard === true) {
            return <Redirect to='/' />
        }
        return (
            <nav className="navbar navbar-expand navbar-dark bg-dark static-top">
                <TitleComponent title="Metric Tree Labs Code Test"></TitleComponent>

                <h6 className="navbar-brand mr-1">Metric Tree Labs Code Test</h6>
            </nav>
        );
    }
}

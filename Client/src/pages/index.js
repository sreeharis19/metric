import React, { Component } from 'react';
import Header from "../elements/header";
import {Redirect} from 'react-router-dom';
import axios from 'axios';

export default class Index extends Component {
    state = {
        files: [],
        toDashboard: false,
        isLoading: false
    };

    constructor(props) {
        super(props);
        this.url = 'http://localhost:5001/api/v1/';
        this.token = localStorage.getItem('token');
    }

    componentDidMount() {
        axios.get(this.url + 'users/getalluserslist', { headers: { authorization: `Bearer ${this.token}` } })
            .then(response => {
                console.log(response)
                const files = response.data;
                this.setState({ files: files });
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleClickDelete = event => {
        axios.delete(this.url + '/' + event.target.value , { params: { token: this.token}})
            .then(response => {
                this.componentDidMount();
                this.setState({ isLoading: true})
            })
            .catch( error => {
                console.log(error.toString());
                this.setState({ toDashboard: true });
            });
    };

    render() {
        if (this.state.toDashboard === true) {
            return <Redirect to='/' />
        }
        return (
            <div>
                <Header/>
                <div id="wrapper">
                    <div id="content-wrapper">
                        <div className="container-fluid">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                   Admin
                                </li>
                                <li className="breadcrumb-item active">All Users Detial list</li>
                            </ol>
                            <div className="card mb-3">
                                <div className="card-header"><i className="fas fa-table"></i>
                                    &nbsp;&nbsp;All Users Detial list
                                </div>
                                <div className="card-body">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>File Name</th>
                                            <th>File URL</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.state.files.map((files, index) =>
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{files.url_path}</td>
                                                <td>{files.pdf}</td>
                                                <td className="text-center">
                                                    <a href={files.url_path} className={'btn btn-sm btn-success download' + files.id} >Download &nbsp;&nbsp;&nbsp;
                                                    </a>
                                                </td>
                                            </tr>)
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

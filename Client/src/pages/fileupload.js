import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import Header from "../elements/header";

export default class FileUploadPage extends Component {

    constructor(props) {
        super(props);
        this.file = '';
        this.url = 'http://localhost:5001/api/v1/';
        this.token = localStorage.getItem('token');
        console.log(this.token)
    }

    state = {
        files: [],
        redirect: false,
        isLoading: false,
    };

    componentDidMount() {
        axios.get(this.url + 'users/getuserfiles', { headers: { authorization: `Bearer ${this.token}` } })
            .then(response => {
                console.log(response)
                const files = response.data.data;
                this.setState({ files: files });
            })
            .catch(error => {
                console.log(error);
            });

    }

    handleChange = event => {
        event.preventDefault();
        this.file = event.target.files[0];
        document.getElementById('fileLabel').innerHTML = event.target.files[0].name;
    };

    handleSubmit = event => {
        event.preventDefault();
        this.setState({ isLoading: true });
        let bodyFormData = new FormData();
        bodyFormData.append('file', this.file);
        axios.post(this.url + 'users/upload', bodyFormData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'authorization': `Bearer ${this.token}`,
            }
        })
            .then(result => {
                console.log("result")
                console.log(result)
                if (result.data.status) {
                    this.componentDidMount();
                    this.setState({ redirect: true, isLoading: false });
                    document.getElementById('fileInput').value = "";
                    document.getElementById('fileLabel').innerHTML = "Choose file";
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    getYear() {
        return new Date().getFullYear();
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/fileupload' />
        }
    };

    render() {
        const isLoading = this.state.isLoading;
        return (
            <div>
                <Header />
                <div id="wrapper">
                    <div id="content-wrapper">
                        <div className="container-fluid">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                   User
                                </li>
                                <li className="breadcrumb-item active">File Upload</li>
                            </ol>
                        </div>
                        <div className="container-fluid">
                            <div className="card mx-auto">
                                <div className="card-header">File Upload</div>
                                <div className="card-body">
                                    <form onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col-md-6">
                                                    <div className="input-group input-group-lg">
                                                        <div className="custom-file">
                                                            <input type="file" onChange={this.handleChange} className="custom-file-input" id="fileInput" />
                                                            <label className="custom-file-label" id="fileLabel" htmlFor="fileInput">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <button className="btn btn-primary btn-block" type="submit" disabled={this.state.isLoading ? true : false}>Upload &nbsp;&nbsp;&nbsp;
                                                            {isLoading ? (
                                                                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            ) : (
                                                                <span></span>
                                                            )}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                    {this.renderRedirect()}
                                </div>
                            </div>
                            <div style={{ padding: 10 }}></div>
                            <div className="table">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>File URL</th>
                                            <th>File Name</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.files.map((files, index) =>
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{files.url_path}</td>
                                                <td>{files.pdf}</td>
                                                <td className="text-center">
                                                    <a href={files.url_path} className={'btn btn-sm btn-success download' + files.id} >Download &nbsp;&nbsp;&nbsp;
                                                    </a>
                                                </td>
                                            </tr>)
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



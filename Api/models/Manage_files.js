const mongoose = require('mongoose');

const Manage_filesSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.ObjectId,
        required: true
    },
    pdf: {
        type: String
    },
    url_path: {
        type: String
    },
});

module.exports = mongoose.model('Manage_files', Manage_filesSchema, 'files');

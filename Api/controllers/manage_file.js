const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Manage_files = require('../models/Manage_files');

// @desc      Get all users files

exports.getAllusersfiles = asyncHandler(async (req, res, next) => {
    const files = await Manage_files.find();
    res.status(200).json(files);
});

// @desc      Get single user files

exports.getuserfiles = asyncHandler(async (req, res, next) => {
    const files = await Manage_files.find({ user: `${req.user.id}` })

    if (!files) {
        return next(
            new ErrorResponse(`No files found with id of ${req.user.id}`, 404)
        );
    }

    res.status(200).json({ success: true, data: files });
});

// @desc      Upload file

exports.userFileUpload = asyncHandler(async (req, res, next) => {

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400));
    }
    console.log(req.files.file.mimetype);
    const file = req.files.file;

    // Make sure the file is a pdf
    if (!file.mimetype.match('application/pdf')) {
        return next(new ErrorResponse(`Please upload a pdf file`, 400));
    }

    // Check filesize
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(
                `Please upload an pdf less than ${process.env.MAX_FILE_UPLOAD}`,
                400
            )
        );
    }

    // Create custom filename
    const random = Math.floor(1000 + Math.random() * 9000);
    file.name = `pdf_${random}${path.parse(file.name).ext}`;

    const url_path = `${req.protocol}://${process.env.FILE_HOST}/${process.env.FILE_URL_PATH}/${file.name}`;

    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }

        pdf = file.name;
        user = req.user.id;
        await Manage_files.create({ user, pdf, url_path });

        res.status(200).json({
            success: true,
            data: file.name
        });
    });
});

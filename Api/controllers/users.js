const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Users = require('../models/Users');

// @desc : Get all users
// @route : GET /api/v1/users
//@access : Public
exports.getUsers = asyncHandler(async (req, res, next) => {
    const token = req.headers.jwtToken;
    const users = await Users.find(token);
    res.status(200).json({ sucess: true, data: users });
});

// @desc : Get single user
// @route : GET /api/v1/users/:id
//@access : Public
exports.getUser = asyncHandler(async (req, res, next) => {
    const token = req.headers.jwtToken;
    const user = await Users.findById(token);
    if (!user) {
        return next(new ErrorResponse(`User token mismatch`, 404));
    }
    res.status(200).json({ sucess: true, data: user });
});

// @desc : Create new user
// @route : POST /api/v1/users
//@access : Private
exports.createUser = asyncHandler(async (req, res, next) => {
    const user = await Users.create(req.body);

    res.status(201).json({
        success: true,
        data: user
    });
});

// @desc : Update
// @route : PUT /api/v1/users/:id
//@access : Private
exports.updateUser = asyncHandler(async (req, res, next) => {
    const token = req.headers.jwtToken;
    const user = await Users.findByIdAndUpdate(token, req.body, {
        new: true,
        runValidators: true
    });
    if (!user) {
        return next(new ErrorResponse(`User token mismatch`, 404));
    }
    res.status(200).json({ sucess: true, data: user });
});

// @desc : Delete User
// @route : DELETE /api/v1/users/:id
//@access : Private
exports.deleteUser = asyncHandler(async (req, res, next) => {
    const token = req.headers.jwtToken;
    const user = await Users.findById(token);
    if (!user) {
        return next(new ErrorResponse(`User token mismatch`, 404));
    }
    res.status(200).json({ sucess: true, data: user });
});
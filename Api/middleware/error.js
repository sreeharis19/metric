const ErrorResponse = require("../utils/errorResponse");

const errorHandler = (err, req, res, next) => {
    let error = { ...err };

    error.message = err.message;
    error.statusCode = err.statusCode;

    //Mongoode bad ObjectId
    if (err.name === 'CastError') {
        const message = `Resource not found with id of ${err.value}`;
        const statuscode = 404;
        error = new ErrorResponse(message, statuscode);
    }
    //Mongoose duplicate value
    if (err.code === 11000) {
        const message = `Duplicate Field value entered`;
        const statuscode = 404;
        error = new ErrorResponse(message, statuscode);
    }
    //mongoose validation error
    if (err.name === 'ValidationError') {
        const message = Object.values(err.errors).map(val => val.message);
        const statuscode = 404;
        error = new ErrorResponse(message, statuscode);
    }
    res.status(error.statusCode || 500).json({
        sucess: false,
        error: error.message || 'Server Error'
    });
};

module.exports = errorHandler;
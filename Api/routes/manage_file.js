const express = require('express');
const {
  getAllusersfiles,
  getuserfiles,
  userFileUpload
} = require('../controllers/manage_file.js');


const router = express.Router();

const { protect } = require('../middleware/auth');

router.get('/getalluserslist', protect, getAllusersfiles);
router.get('/getuserfiles', protect, getuserfiles);
router.post('/upload', protect, userFileUpload);


module.exports = router;

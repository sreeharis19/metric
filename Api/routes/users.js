const express = require('express');
const {
    getUsers,
    getUser,
    createUser
} = require('../controllers/users.js');


const router = express.Router({ mergeParams: true });

const { protect } = require('../middleware/auth');

router.use(protect);

router
    .route('/')
    .get(getUsers)
    .post(createUser);

router
    .route('/:id')
    .get(getUser)

module.exports = router;
